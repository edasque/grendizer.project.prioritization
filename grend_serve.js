var JiraApi = require('./inc/jira.js').JiraApi;
var zlib = require('zlib');
var fs = require('fs');
var dateFormat = require('dateformat');

var JIRA_credentials_JSON = fs.readFileSync('./conf/jira.credentials.json', "ascii");
var JIRA_credentials = JSON.parse(JIRA_credentials_JSON);

var jlogin = JIRA_credentials.jlogin
var jpass = JIRA_credentials.jpass
var jhost = JIRA_credentials.jhost
var jport = JIRA_credentials.jport
var JIRA_API_version = JIRA_credentials.JIRA_API_version
var jprotocol = JIRA_credentials.jprotocol

// I am borrowing the global rank from GreenHopper to prioritize tasks
// customfield_10000 -> Global Rank
var global_rank = JIRA_credentials.global_rank

var jira = new JiraApi(jprotocol, jhost, jport, jlogin, jpass, JIRA_API_version);

var a_fields = ["summary", "status", "timetracking", "labels", "assignee", "duedate", global_rank];

var userList = new Array();

var io = require('socket.io').listen(5701);

// We should do this every 24 hours or something.
retrieveUserList();

io.set('log level', 1)

io.sockets.on('connection', function(socket) {

  socket.emit('load_users', userList);

  // getQueue("backend_queue");
  // getQueue("frontend_queue");
  getQueue();


  socket.on('identified_as', function(data) {

    console.log(dateFormat(new Date(), "isoDateTime") + ": User Identified as: "+data.username);

  });

  socket.on('refresh_queues', function(data) {

    getQueue();
    console.dir(dateFormat(new Date(), "isoDateTime") + ": Refreshing queues");


  });

  socket.on('priority_change_order', function(data) {
    
    console.log(dateFormat(new Date(), "isoDateTime") + ": Changing issue priority: "+JSON.stringify(data) )
    jira.changeIssueRank(data.put, data.between, data.and, 10000, function(err, data_changeIssueRank) {
      if(err) console.dir(err);
      else {
        // console.dir(data_changeIssueRank)
      console.log(dateFormat(new Date(), "isoDateTime") + ": Changed issue priority: "+JSON.stringify(data) )

        jira.commentOnIssue(data.put, "Prioritization was changed for this feature in Grendizer (http://grendizer.hungryfi.sh:5700/) by " + data.by, function(err, data_comment) {
          if(err) console.dir(err);
          else {
            // console.dir(data_comment);
          };
        });



      };


    });
  });

  socket.on('received_queue', function(data) {
    console.log(dateFormat(new Date(), "isoDateTime") + ": Client received "+data.queue_name+" queue");
  });

  socket.on('addLabel', function(input) {

    jira.addLabel(input.issue, input.label, function(err, data) {

      if(err) console.dir(err);
      else {

        getQueue();

        socket.emit('added_label', input);


      }
    });
  });


});


function retrieveUserList() {
  jira.retrieveUserList("TECH", function(err, users) {

    if(err) console.dir(err);
    else {

      for(index_user in users) {
        userList.push(users[index_user].displayName);
      }
      console.log(dateFormat(new Date(), "isoDateTime") + ": Retrieved %s users from JIRA", userList.length);
    }


  });

}


function getQueue() {

  jira.searchJira('(labels = backend_queue or labels = frontend_queue) AND issuetype not in subTaskIssueTypes()  and status != "Closed" ORDER BY "Global Rank"', a_fields, returnTQueue)

}

function returnTQueue(err, query_result) {

  if(err) {
            console.dir(err);
            console.log("Trying again");
              jira.searchJira('(labels = backend_queue or labels = frontend_queue) AND issuetype not in subTaskIssueTypes()  and status != "Closed" ORDER BY "Global Rank"', a_fields, returnTQueue)

          }
  else {

    //console.dir(query_result)
    io.sockets.emit('load_tech_queue', {
      issues: query_result.issues


    });
  }
}


var http = require("http"),
  url = require("url"),
  path = require("path"),
  port = process.argv[2] || 5700;

http.createServer(function(request, response) {

  var filename =   path.resolve(__dirname) + "/assets" + url.parse(request.url).pathname;
   


  console.log(dateFormat(new Date(), "isoDateTime") + ": serving " + filename + " to " + JSON.stringify(request.connection.address()) + "");

  // if the file doesn't exist at all, 404
  if(!fs.existsSync(filename)) {
              response.writeHead(404, {
        "Content-Type": "text/plain"
      });
      response.write("404 Not Found\n");
      response.end();
      return;
      }

  //if the file is a directory and index.html in it exists, then go ahead
  if(fs.statSync(filename).isDirectory())
    {
      if (fs.existsSync(filename+'/index.html')) filename += '/index.html';
      else
      {
              response.writeHead(404, {
        "Content-Type": "text/plain"
      });
      response.write("404 Not Found\n");
      response.end();
      return;
      }
    }



  fs.exists(filename, function(exists) {
    if(!exists) {
      response.writeHead(404, {
        "Content-Type": "text/plain"
      });
      response.write("404 Not Found\n");
      response.end();
      return;
    }


    var raw = fs.createReadStream(filename);
    var acceptEncoding = request.headers['accept-encoding'];
    if(!acceptEncoding) {
      acceptEncoding = '';
    }


    if(acceptEncoding.match(/\bdeflate\b/)) {
      response.writeHead(200, {
        'content-encoding': 'deflate'
      });
      raw.pipe(zlib.createDeflate()).pipe(response);
    } else if(acceptEncoding.match(/\bgzip\b/)) {
      response.writeHead(200, {
        'content-encoding': 'gzip'
      });
      raw.pipe(zlib.createGzip()).pipe(response);
    } else {
      response.writeHead(200, {});
      raw.pipe(response);
    }



  });
}).listen(parseInt(port, 10));

console.log(dateFormat(new Date(), "isoDateTime") + ": Static file server running at\n  => http://localhost:" + port + "/\nCTRL + C to shutdown");
