var socket = io.connect('http://' + window.location.hostname + ":5701");

$.extend($.fn.dataTableExt.oStdClasses, {
  "sWrapper": "dataTables_wrapper form-inline"
});
$.extend($.fn.dataTable.defaults, {
  "bPaginate": false,
  "bLengthChange": false,
  "bFilter": true,
  "bSort": false,
  "bInfo": true,
  "bAutoWidth": false
});


function enableDND() {


  $(".issues_list").tableDnD({
    onDrop: function(table, row) {

      var rows = table.tBodies[0].rows;
      var debugStr = "Row dropped was " + row.id;

      /*". New order: ";
            for (var i=0; i<rows.length; i++) {
                debugStr += rows[i].id+" ";
            }*/

      var row_elem = $(row);
      var row_previous = row_elem.prev();
      var row_next = row_elem.next();

      debugStr += ", between " + row_previous.attr('id') + " and " + row_next.attr('id');

      // $("#debugArea").html(debugStr);
      console.log(debugStr);

      var priority_change_order = {
        put: row_elem.attr('id'),
        between: row_previous.attr('id'),
        and: row_next.attr('id'),
        by: $("#usersearch").val(),
        queue: $(table).attr('id')
      };
      console.dir(priority_change_order);


      socket.emit('priority_change_order', priority_change_order);

    },
  }


  );
}

$(document).ready(function() {



  $("#bLogin").click(
    function() { 
      socket.emit('identified_as', {"username":$("#usersearch").val()});

      enableDND();


      }
        );



  $("#bRefresh").click(function() {
    socket.emit('refresh_queues', null);
  });
  $("#btech_queue_add").click(function() {
    if($('#issue_to_add').val()) socket.emit('addLabel', {
      "issue": $('#issue_to_add').val(),
      "label": $('#tech_queue_select').val()
    });
    else console.log("Issue ID can't be empty");
  });



  socket.on('load_users', function(users) {
    console.log("USERS:");
    console.dir(users);
    $('#usersearch').typeahead({
      source: users
    });

  });

  socket.on('added_label', function(payload) {
    console.log("Added Label:");
    console.dir(payload);
  });

  socket.on('load_tech_queue', function(data) {

    console.log("Loading Tech Queue, building table");

    var issues = data.issues;
    var queue_table = $("#tech_queue tbody");

    queue_table.html("");

    for(issue_index in issues) {

      var current_issue = issues[issue_index];


      var status_class = "";

      switch(current_issue.fields.status.name) {
      case "In Progress":
        status_class = "in_progress";
        break;
      case "Resolved":
        status_class = "resolved";
        break;
      }

      // console.dir(current_issue.fields);
      var estimate = current_issue.fields.timetracking.originalEstimate===undefined?"-":current_issue.fields.timetracking.originalEstimate;

      var label = ($.inArray("backend_queue", current_issue.fields.labels) != -1 ? "Back End" : (($.inArray("frontend_queue", current_issue.fields.labels) != -1) ? "Front End" : "-"));

      current_issue.fields.duedate = (current_issue.fields.duedate) ? current_issue.fields.duedate : "None set";
      queue_table.append("<tr id='" + current_issue.key + "' class='" + status_class + "'><td><a href='https://hungryfishmedia.jira.com/browse/" + current_issue.key + "' target='_blank'>" + current_issue.key + "</a></td><td>" + current_issue.fields.summary + "</td><td>" + current_issue.fields.assignee.displayName + "</td><td>" + current_issue.fields.duedate + "</td><td>" + estimate + "</td><td>" + label + "</td></tr>");

    }


    $("#tech_queue").dataTable();
    if ($("#usersearch").val()) enableDND();
  $(".alert").alert('close')


    socket.emit('received_queue', {"queue_name":"Backend & Frontend"});
  });


});