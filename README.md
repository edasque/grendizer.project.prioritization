Grendizer Project Prioritization
==================

This is a web application which makes it easy to prioritize your projects in JIRA. To get it working you'll need to :

* copy `conf/jira.credentials.json.example` into `conf/jira.credentials.json.example` and set up your configuration
	* `jlogin`: a user login (typically use an API-only user but you may use a normal user)
	* `jpass` : the user password
	* `jhost` : the instance's host name, something like "company.jira.com"
	* `jport` : typically "80"
	* `jprotocol` : typically "https"
	* `JIRA_API_version` : currently "2"
	* `global_rank` : used to hold the Prioritization. Typically the greenhopper custom field, "customfield_10000"

* Use `backend_queue` and `frontend_queue` to tag issues you'll want to prioritize. This can be changed easily in the source code.

Running the server
-------------------

typically, to run the application, with node installed, you'd:

	node grend_serve.js.js

and hit the application at [http://localhost:5700/](http://localhost:5700/)

Usage
-------------------


![Alt text](https://bitbucket.org/edasque/grendizer.project.prioritization/raw/80108cb544ce38468cf46cc2e4418cb7be2c43a3/assets/images/Screenshot_2_26_13_3_29_PM.png "Optional title")
Pre-requisites
--------------
You'll need have node.js installed as well as a few modules, including:
* dateformat
* zlib

	simply running this should do the job :
	> npm install dateformat zlib

